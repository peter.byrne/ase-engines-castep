from pathlib import Path
import shutil

from ase.build import bulk
from ase.calculators.castep_generic import (
    CastepProfile, CastepTemplate)

class CastepCalculation:
    def __init__(self, atoms, parameters, directory):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory.resolve()
        self.checkpoint = self.directory / "castep.check"
        
    def read_results(self):
        template = CastepTemplate()
        return template.read_results(self.directory)
    
def groundstate(atoms, parameters, directory):
    """
    Calculate the ground state and return an object representing the ground state checkpoint
    Seems to work!
    """
    profile = CastepProfile()
    template = CastepTemplate()
    directory.mkdir(exist_ok=True, parents=True)
    template.write_input(directory, atoms, parameters, ['energy'])
    template.execute(directory, profile)
    return CastepCalculation(atoms, parameters, directory)

def bandstructure(gs, bandpath, parameters, directory):
    atoms = gs.atoms.copy()
    directory.mkdir(exist_ok=True, parents=True)

    bs_parameters = parameters.copy()
    bs_parameters['param'].update(continuation=gs.checkpoint,
                                  task='spectral',
                                  spectral_task='bandstructure')
    bs_parameters['cell'].update(spectral_kpoint_list=bandpath.kpts)
    profile = CastepProfile()
    template = CastepTemplate()
    template.write_input(directory, atoms, bs_parameters, ['energy'])    
    template.execute(directory, profile)
    return CastepCalculation(atoms, bs_parameters, directory)


# from ase.optimize.bfgs import BFGS
# def relax(atoms, parameters, directory):
#     minimizer = BFGS(atoms)
#     minimizer.run(fmax=0.02)


def main():
    directory = Path('work/castep')
    atoms = bulk('Si')

    params = {
        'param': {'cutoff_energy':  '100 eV'},
        'cell': {},
        'kpts': [2,2,2]
        }

    gs = groundstate(atoms, params, directory / 'groundstate')
    results = gs.read_results()
    print(results['forces']) # works!
    
    bandpath = atoms.cell.bandpath('GXWK', density=15)
    bscalc = bandstructure(gs, bandpath, params, directory / 'bandstructure')
    results = bscalc.read_results()
    
    eigenvalues = results['eigenvalues']
    castep_kpts = results['kpts']
    efermi = results['efermi']

    # Check the kpoints that we read were within tolerance
    kpts_err = abs(bandpath.kpts - castep_kpts).max()
    assert kpts_err < 1e-7, f"Error in read kpoints: {kpts_err}"

    # Plot a bandstructure
    from ase.spectrum.band_structure import BandStructure
    bs = BandStructure(bandpath, eigenvalues, efermi)
    bs.write(directory / 'bs.json')

if __name__ == '__main__':
    main()
