# """The GW calculation template for AIMS
# """
# import os
# from ase.calculators.aims import Aims

# from aims import check_directory, AimsCalculation


# def gw(atoms, parameters, directory, overwrite=False):
#     """A GW calculation for aims

#     Args:
#         atoms (ase.Atoms): a relaxed Atoms object to calculate GW for
#         parameters (dict): general parameters for the calculation
#         directory (:obj:`list` or :obj:`pathlib.Path`): a directory to put the calculation into
#         overwrite (boolean): a directory overwrite flag

#     Returns:
#         an AimsCalculation object
#     """

#     directory = check_directory(directory, overwrite)

#     # checks
#     if sum(atoms.pbc) not in (0, 3):
#         raise ValueError(
#             "GW calculation can be perform only on bulk or cluster geometry"
#         )

#     if all(atoms.pbc):
#         if "qpe_calc" not in parameters:
#             parameters["qpe_calc"] = "gw_expt"
#         elif parameters["qpe_calc"] != "gw_expt":
#             raise ValueError("`qpe_calc` has to be `gw_expt` for periodic calculations")
#     else:
#         if "qpe_calc" not in parameters:
#             parameters["qpe_calc"] = "gw"
#         elif ("sc_self_energy" not in parameters) or (
#             parameters["qpe_calc"] not in ("gw", "ev_scgw", "ev_scgw0", "mp2")
#         ):
#             raise ValueError("`qpe_calc` is not correct for cluster calculation")

#     if ("anacon_type" not in parameters) or (
#         str(parameters["anacon_type"]) not in ("two-pole", "pade", "0", "1")
#     ):
#         raise KeyError(
#             "GW calc should have `anacon_type` parameter with the value from [`two-pole`, `pade`]"
#         )

#     calc = Aims(directory=directory, **parameters)
#     calc.calculate(atoms, ["energy"], None)
#     return AimsCalculation(atoms, parameters, directory)


# def main():
#     from pathlib import Path
#     from tempfile import TemporaryDirectory
#     from ase.build import bulk

#     atoms = bulk("Li")
#     parameters = {
#         "xc": "pbe",
#         "k_grid": [4, 4, 4],
#         "qpe_calc": "gw_expt",
#         "anacon_type": "two-pole",
#     }
#     with TemporaryDirectory() as temp_dir:
#         gw(atoms, parameters, temp_dir)
#         with open(Path(temp_dir) / "control.in") as f:
#             print(f.read())


# if __name__ == "__main__":
#     main()
