""" A collection of tests for GW AIMS calculation
"""

import pytest

from ase.build import bulk, molecule

from .aims import gw


@pytest.fixture
def Li():
    return bulk("Li")


@pytest.fixture
def molecule():
    return molecule("C2H4")


@pytest.fixture
def parameters():
    return {
        "species_dir": "/home/andrey/workspace/FHIaims/species_defaults/defaults_2020/light",
        "run_command": "mpirun -np 4 aims.x",
        "xc": "pbe",
        "k_grid": [4, 4, 4],
    }


def test_1d_2d(parameters):
    """Test against 1d and 2d structures"""
    from ase.build import nanotube, bcc100

    with pytest.raises(ValueError):
        atoms = nanotube(3, 3)
        gw(atoms, parameters, ".")

    with pytest.raises(ValueError):
        atoms = bcc100("Li", [2, 2, 2])
        gw(atoms, parameters, ".")


def test_3d_error(Li, parameters):
    """Test bulk structure"""
    parameters["qpe_calc"] = "gw"
    with pytest.raises(ValueError):
        gw(Li, parameters, ".")
